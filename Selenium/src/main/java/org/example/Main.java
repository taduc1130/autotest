package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;

public class Main {
    public static void main(String[] args) {
        //Start the session
        WebDriver driver = new ChromeDriver();

        //Take action on browser
        driver.get("https://mlearning.hoasen.edu.vn/");

        //Request browser information
//            driver.getTitle();

        //Establish waiting strategy
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(2000));
        System.out.println(driver.getTitle());

        //Find an element

        WebElement textBox = driver.findElement(By.name("username"));
        WebElement textBox1 = driver.findElement(By.name("password"));

        WebElement submitButton = driver.findElement(By.id("submit"));

        //Take action on element
        textBox.sendKeys("DUC.TM1359@SINHVIEN.HOASEN.EDU.VN");
        textBox1.sendKeys("Conchongu0123");

        submitButton.click();
        //Request element information
        //End the session

        // Identify the anchor element using its className
        String anchorClassName = "dropdown-toggle"; // Replace with the actual className of the anchor element

        // Find the anchor element
        WebElement anchorElement = driver.findElement(By.className(anchorClassName));

        // Perform an action on the anchor element, e.g., click
        anchorElement.click();

        WebElement logoutButton = driver.findElement(By.id("actionmenuaction-6"));
        logoutButton.click();

        driver.quit();
        System.out.println("hello 1");

    }
}